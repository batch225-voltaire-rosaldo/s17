/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function myFunction1() {
	let name = prompt("What is your name?");
	let age = prompt("How old are you?");
	let add = prompt("Where do you live?");

	console.log("Hello, " + name);
	console.log("You are " + age + " years old.");
	console.log("You live in " + add);
	alert("Thank you for your input!");
}
myFunction1();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function myFunction2() {

	let band1 = "Smashing Pumpkins";
	let band2 = "Coldplay";
	let band3 = "Maroon 5";
	let band4 = "Incubus";
	let band5 = "Lenny Kravits";

	console.log("1. " + band1); 
	console.log("2. " + band2);
	console.log("3. " + band3);
	console.log("4. " + band4);
	console.log("5. " + band5);
}
myFunction2();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function myFunction3() {

	let movie1 = "Harry Potter";
	let movie2 = "Lord of the Rings";
	let movie3 = "Hunger Games";
	let movie4 = "Avengers";
	let movie5 = "Spider Man";
	let movie1rate = 100;
	let movie2rate = 90;
	let movie3rate = 95;
	let movie4rate = 98;
	let movie5rate = 96;

	console.log("1. " + movie1);
	console.log("Rotten Tomatoes Rating: " + movie1rate + "%");
	console.log("2. " + movie2);
	console.log("Rotten Tomatoes Rating: " + movie2rate + "%");
	console.log("3. " + movie3);
	console.log("Rotten Tomatoes Rating: " + movie3rate + "%");
	console.log("4. " + movie4);
	console.log("Rotten Tomatoes Rating: " + movie4rate + "%");
	console.log("5. " + movie5);
	console.log("Rotten Tomatoes Rating: " + movie5rate + "%");
}
myFunction3();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printFriends();
// console.log(friend1);
// console.log(friend2);































