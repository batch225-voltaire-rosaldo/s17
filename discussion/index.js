// Basic Functions

// [Section] Function Declaration

/*
	The statements and instructions inside a function is not immediately executed when the function is defined.
	They are run/executed when a function is invoked.
	To invoke a declared function, add the name of the function and a parenthesis.

*/
// Hoisting - javascript behavior for certain variables and functions to run to use them before their declaration

function printName() {
	console.log("My name is John");
}

printName();

// Function Expression

let variableFunction = function myGreetings() {
	console.log("Hello");
	// myGreetings();
}

// Re-assigning Functions

variableFunction = function myGreetings() {
	console.log("Updated Hello");
}

variableFunction();

// Function Scoping

// Scope is the accessibility of variables
	// local/block scope
	// global scope
	// function scope

// Local Variable

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

// Global Variable
let globalVar = "Mr. Worldwide";

console.log(globalVar);

// Function Scope

function showNames() {
	// Function scoped variables:

	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionConst);
	console.log(functionLet);

}

showNames();

// console.log(functionConst); // Result an error
// console.log(functionLet);

// alert() and prompt()

// alert() - allows us to show a small window at the top of our browser page to show information to our users.

// It, much like alert(), will have the page wait until the user completes or enters their input.

// alert("Hello, user!");

// function showSampleAlert() {
// 	alert("Hello User!!!");
// }

// showSampleAlert();

// ======================================

// Prompt() - Allows us to show a small window at the top of the browser to gather user input.

// let samplePrompt = prompt("Enter your name: ");

// console.log("Hello, " + samplePrompt);
// prompt()

function printWelcomeMessages() {
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to DOTA2");
}

printWelcomeMessages();
































